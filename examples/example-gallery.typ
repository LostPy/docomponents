#import "docomponents.typ": *

= Introduction

#lorem(40)

#doc-code()[
  ```py
  def main():
      print("Hello World!")
  ```
]

#doc-code(
  explaination: lorem(20),
  cap: "Hello world example in Rust.",
  label: "Rust function",
  dark-style: true,
)[
  ```rs
  fn main() {
      println!("Hello world!")
  }
  ```
]

#lorem(60)

#doc-info(lorem(40))

#doc-question(lorem(40))

#doc-warning(lorem(50))

#lorem(50)

#doc-error(lorem(30))

#doc-info(lorem(20) + doc-error(lorem(10)))

#doc-success(lorem(10))

#doc-code(
  cap: "The code for success box.",
  altern-color: true,
  line-numbering: true,
)[
  ```typst
  #let doc-success(
    title: strong("Success"),
    content,
  ) = {
    doc-box(
      fill: rgb(178, 242, 187),
      stroke-color: rgb(47, 158, 68),
      title: title,
      icon: "im/success.svg",
      content,
    )
  }
  ```
]
