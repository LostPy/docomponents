# DoComponents

**DoComponents** (Documentation Components) is a [Typst](https://github.com/typst/typst) package to provide a set of nice components for your documentation.

The following components are currently available:

 * `doc-code`: Display a code block in light or dark style, with numbering lines and/or with a explanation.
 * `doc-success`: Display a "success" message in a green box.
 * `doc-error`: Display an "error" message in a red box.
 * `doc-warning`: Display a "warning" message in an orange box.
 * `doc-info`: Display a "success" message in a teal box.
 * `doc-question`: Display a "success" message in a blue box.

## Usage

```
#import "docomponents.typ": *

# doc-success(lorem(30))
```