#let doc-code-counter = counter("code")
#let doc-code(
  body,
  cap: none,
  label: <code>,
  explaination: none,
  line-numbering: false,
  dark-style: false,
  altern-color: false,
) = {
  set par(first-line-indent: 0pt) 
  let text-color = if dark-style {white} else {black}

  let content = ()
  if body.has("children") {
    for item in body.children {
      if item.func() == raw {
        for (i, line) in item.text.split("\n").enumerate() {
          if line-numbering {
            content.push(text(str(i + 1), fill: text-color))
          } else {content.push("")}
          content.push(text(
            raw(
              line,
              lang: if item.has("lang") {item.lang} else {none}),
              fill: text-color
          ))
        }
      }
    }
  }

  let codebox = box(
    stroke: 2pt + luma(60),
    radius: if explaination != none { (top: 5pt) } else {5pt},
    inset: 5pt,
    fill: if dark-style {luma(60)} else {rgb(99%, 99%, 99%)},
    width: 0.8fr
  )[
    #set align(left)
    #table(
      columns: (auto, 1fr),
      inset: 5pt,
      stroke: none,
      fill: (_, row) => {
        if dark-style and calc.odd(row) and altern-color {
          luma(70)
        } else if dark-style {
          luma(60)
        } else if calc.odd(row) and altern-color {
          luma(240)
        } else {
          white
        }
      },
      align: horizon,
      ..content
    )
  ]
  if cap == none {
    box(width: 100%)[
      #if explaination != none {
        stack(
          dir: ttb,
          codebox,
          [
            #box(
              stroke: 2pt + luma(60),
              radius: (bottom: 5pt),
              inset: 10pt,
              width: 100%,
            )[#explaination]
          ]
        )
      } else {
        codebox
      }
    ]
  } else {
    [
      #figure(
        if explaination != none {
          stack(
            dir: ttb,
            codebox,
            [
              #box(
                stroke: 2pt + luma(60),
                radius: (bottom: 5pt),
                inset: 10pt,
                width: 100%,
              )[#explaination]
            ]
          )
        } else {
          codebox
        },
        caption: cap,
        kind: "code",
        supplement: "Code"
      )
      #label
    ]
  }
}
