#let doc-box(
  title: none,
  icon: none,
  fill: luma(50),
  stroke-color: luma(30),
  inset: 16pt, 
  outset: 0pt, 
  content
) = {
  let radius = 3pt
  set block(spacing: 0pt)

  pad(
    y: inset,
    [
      #pad(
        x: outset,
        rect(
          width: 100%,
          inset: 0pt,
          radius: radius,
          stroke: (
            left: 3pt + stroke-color,
            top: 3pt + stroke-color,
            right: 1pt + stroke-color,
            bottom: 1pt + stroke-color,
          ),
          fill: fill,
          [
            #if title != none or icon != none {
              block(
                width: 100%,
                radius: (top: radius),
                fill: stroke-color,
                pad(
                  5pt,
                  if icon != none and title != none {
                    stack(dir: ltr, image(icon, width: 15pt), 5pt, v(3pt) + text(title, white))
                  } else if title != none {
                    text(white, title)
                  } else {
                    image(icon, width: 15pt)
                  }
                )
              )
            }
            #pad(
              10pt,
              par(justify: false, first-line-indent: 0pt, content)
            )
          ]
        )
      )
    ]
  )
}


#let doc-info(
  title: strong("Info"),
  content,
) = {
  doc-box(
    fill: rgb(197, 246, 250),
    stroke-color: rgb(16, 152, 173),
    title: title,
    icon: "im/info.svg",
    content,
  )
}

#let doc-question(
  title: strong("Question"),
  content,
) = {
  doc-box(
    fill: rgb(208, 235, 255),
    stroke-color: rgb(34, 139, 230),
    title: title,
    icon: "im/question.svg",
    content,
  )
}

#let doc-warning(
  title: strong("Warning"),
  content,
) = {
  doc-box(
    fill: rgb(255, 243, 191),
    stroke-color: rgb(240, 140, 0),
    title: title,
    icon: "im/warning.svg",
    content,
  )
}

#let doc-error(
  title: strong("Error"),
  content,
) = {
  doc-box(
    fill: rgb(255, 201, 201),
    stroke-color: rgb(240, 62, 62),
    title: title,
    icon: "im/error.svg",
    content,
  )
}

#let doc-success(
  title: strong("Success"),
  content,
) = {
  doc-box(
    fill: rgb(178, 242, 187),
    stroke-color: rgb(47, 158, 68),
    title: title,
    icon: "im/success.svg",
    content,
  )
}

